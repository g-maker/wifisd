## Prusa插件安装(windows系统)
1.  下载传输软件 [软件V1.3.10](https://gitee.com/g-maker/wifisd/releases/download/v1.3.10/WifiBox.exe),保存到任意目录(最好路径不要包含中文和空格)

2. 找到下载的文件,如图右键后复制文件地址

![搜索到](img/copyfile.png "搜索到")

3.  打开Prusa软件找到如下图所示设置,如果没有找到点击右上角的专家模式

![prusa设置](img/prusa-win.png "prusa设置")

 **注意保存文件对话框完成才会弹出此界面** 

![prusa传输](img/prusa-chuanshu.png "搜索到")

首次使用或者添加新设备可先点击搜索按钮,然后在下拉框中选择要传输的设备,输入文件名后点击确定传输即可
